# Uta Time

## Project Description

Written in Python with the Kivy Framework. It was originally an app for kids to push buttons with and it would play songs. Please do as you wish. It makes me very happy if my code is useful for someone else.

## Contributing
Please see the contributing guide. In short, as long as you use the same MIT license, you will be fine.

## Authors and acknowledgment
Metalinux is the author.

## License
MIT License.
