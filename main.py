__version__="2.1.10"
import os, sys, plyer
os.environ["KIVY_IMAGE"] = 'pil,sdl2'
from kivy.uix.label import Label
from kivy.app import App
from kivy.graphics import Color, Rectangle
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.lang import Builder
from kivy.core.audio import SoundLoader
from kivy.network.urlrequest import UrlRequest
from kivy.config import Config
Config.set('graphics','resizable',True)

class LabelWrap(Label):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.bind(
            width=lambda *x:
            self.setter('text_size')(self, (self.width, None)),
            texture_size=lambda *x: self.setter('height')(self, self.texture_size[1])
        )

class RootWidget(FloatLayout):
   
	def __init__(self, **kwargs):
        # make sure no important functionality is being removed
		super(RootWidget, self).__init__(**kwargs)
             
		# add widget to layout
		title_background = Button(
		background_color = (150,100,50),
		size_hint = (4, 4),
		pos_hint={'center_x':0.44, 'center_y': 0.4})
		self.add_widget(title_background)
				
		japan_flag = Button(
		background_normal= 'images/japan-flag.png',
		size_hint = (0.06, 0.06),
		pos_hint={'center_x': 0.44, 'center_y': 0.87})
		self.add_widget(japan_flag)

		union_jack = Button(
		background_normal = 'images/union-jack.png',
		size_hint = (0.06, 0.06),
		pos_hint = {'center_x': 0.53, 'center_y': 0.87})
		self.add_widget(union_jack)
        
		happy_btn = Button(
        font_size=32,
        text='If You\'re Happy \nAnd You Know It',
        background_color = (0,0,1,1),
        background_normal = 'images/happy.png',
        size_hint=(.3, .3),
        pos_hint={'center_x': .2, 'center_y': .5})
		self.add_widget(happy_btn)
		happy_btn.bind(on_press=self.happy_song)   
        
		sunshine_btn = Button(
        font_size=32,
        text='You Are \nMy Sunshine',
        color = (0,0,0),
        background_color = (255,255,0,1),
        background_down = 'images/sun.png',
        size_hint=(.3, .3),
        pos_hint={'center_x': .8, 'center_y': .5})
		self.add_widget(sunshine_btn)
		sunshine_btn.bind(on_press=self.sunshine_song)
        
		bus_btn = Button(
        font_size=32,
        text='Wheels On \nThe Bus',
        background_color = (255,0,0),
        background_down = 'images/bus.png',
        size_hint=(.3, .3),
        pos_hint={'center_x': .2, 'center_y': .2})   
		self.add_widget(bus_btn)
		bus_btn.bind(on_press=self.bus_song)
        
		rain_btn = Button(
        font_size=32,
        text='I Can Sing \nA Rainbow',
        background_color = (255,0,255),
        background_down = 'images/rainbow.png',
        size_hint=(.3, .3),
        pos_hint={'center_x': .8, 'center_y': .2})
		self.add_widget(rain_btn)
		rain_btn.bind(on_press=self.rain_song)

		horse_btn = Button(
        font_size=32,
        text='Horsey Horsey',
        color = (0,0,0),
        background_color = (0,255,255),
        background_down = 'images/horse.png',
        size_hint=(.3, .3),
        pos_hint={'center_x': .2, 'center_y': .8})
		self.add_widget(horse_btn)
		horse_btn.bind(on_press=self.horse_song)
        
		thunder_btn = Button(
        font_size=32,
        text='I Hear Thunder',
        background_color = (0,0,0),
        background_down = 'images/thunder.png',
        size_hint=(.3, .3),
        pos_hint={'center_x': .8, 'center_y': .8})
		self.add_widget(thunder_btn)
		thunder_btn.bind(on_press=self.thunder_song)
        
		frog_btn = Button(
        font_size=32,
        text='Five Little \nSpeckled Frogs',
        color = (0,0,0),
        background_color = (0,255,0),
        background_down = 'images/frog.png',
        size_hint=(.3, .3),
        pos_hint={'center_x': .5, 'center_y': .5})
		self.add_widget(frog_btn)
		frog_btn.bind(on_press=self.frog_song)
        
		teapot_btn = Button(
		font_size=32,
		text='I\'m A \nLittle Teapot',
		background_color = (0,0,0,0.5),
		background_down = 'images/teapot.png',
		size_hint=(.3, .3),
        pos_hint={'center_x': .5, 'center_y': .8})
		self.add_widget(teapot_btn)
		teapot_btn.bind(on_press=self.teapot_song)
        
		way_btn = Button(
		font_size=32,
		text='This Is The Way',
		background_color = (0,0,128),
		background_down = 'images/way.png',
		size_hint=(.3, .3),
		pos_hint={'center_x': .5, 'center_y': .2})
		self.add_widget(way_btn)
		way_btn.bind(on_press=self.way_song)
		
		stop_audio_btn = Button(
		font_size=32,
		text='Stop Song',
		size_hint=(.2, .06),
		pos_hint={'center_x': .8, 'center_y': 0.98})
		self.add_widget(stop_audio_btn)
		stop_audio_btn.bind(on_press=self.stop_audio)
		
		u = Label(
		text="U",
		bold=True,
		font_size=40,
		size_hint=(.3, .3),
		pos_hint={'center_x': 0.4, 'center_y': 0.92},
		color=(1,1,1,1))
		self.add_widget(u)
		
		t = Label(
		text="T",
		bold=True,
		font_size=40,
		size_hint=(.3, .3),
		pos_hint={'center_x': 0.43, 'center_y': 0.92},
		color=(1,1,1,1))
		self.add_widget(t)
		
		a = Label(
		text="A",
		bold=True,
		font_size=40,
		size_hint=(.3, .3),
		pos_hint={'center_x': 0.46, 'center_y': 0.92},
		color=(1,1,1,1))
		self.add_widget(a)
		
		t = Label(
		text="T",
		bold=True,
		font_size=40,
		size_hint=(.3, .3),
		pos_hint={'center_x': 0.49, 'center_y': 0.92},
		color=(1,1,1))
		self.add_widget(t)
		
		i = Label(
		text="I",
		bold=True,
		font_size=40,
		size_hint=(.3, .3),
		pos_hint={'center_x': 0.52, 'center_y': 0.92},
		color=(1,1,1))
		self.add_widget(i)
		
		m = Label(
		text="M",
		bold=True,
		font_size=40,
		size_hint=(.3, .3),
		pos_hint={'center_x': 0.55, 'center_y': 0.92},
		color=(1,1,1))
		self.add_widget(m)
		
		e = Label(
		text="E",
		bold=True,
		font_size=40,
		size_hint=(.3, .3),
		pos_hint={'center_x': 0.58, 'center_y': 0.92},
		color=(1,1,1))
		self.add_widget(e)
		
		exclaim = Label(
		text="!",
		bold=True,
		font_size=40,
		size_hint=(.3, .3),
		pos_hint={'center_x': 0.61, 'center_y': 0.92},
		color=(150,100,50))
		self.add_widget(exclaim)
        
	def happy_song(self, widget):
		self.sound = SoundLoader.load('songs/happy.wav')
		if self.sound:
			self.sound.play()
    
	def sunshine_song(self, obj):
		self.sound = SoundLoader.load('songs/sunshine.wav')
		if self.sound:
			self.sound.play()
    
	def bus_song(self, obj):
		self.sound = SoundLoader.load('songs/bus.wav')
		if self.sound:
			self.sound.play()
	
	def rain_song(self, obj):
		self.sound = SoundLoader.load('songs/rainbow.wav')
		if self.sound:
			self.sound.play()
            
	def horse_song(self, obj):
		self.sound = SoundLoader.load('songs/horse.wav')
		if self.sound:
			self.sound.play()
	
	def thunder_song(self, obj):
		self.sound = SoundLoader.load('songs/thunder.wav')
		if self.sound:
			self.sound.play()
	
	def frog_song(self, obj):
		self.sound = SoundLoader.load('songs/frog.wav')
		if self.sound:
			self.sound.play()       
	
	def teapot_song(self, obj):
		self.sound = SoundLoader.load('songs/teapot.wav')
		if self.sound:
			self.sound.play()
	
	def way_song(self, obj):
		self.sound = SoundLoader.load('songs/way.wav')
		if self.sound:
			self.sound.play()
	
	def stop_audio(self, obj):
		if self.sound:
			self.sound.stop()

class MainApp(App):

    def build(self):
        
        self.root = root = RootWidget()
        with root.canvas.before:
            Color(1, 1, 1, 1)
            self.rect = Rectangle(size=root.size, pos=root.pos)
        return root
    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size

if __name__ == '__main__':
    MainApp().run()
